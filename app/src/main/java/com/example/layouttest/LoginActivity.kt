package com.example.layouttest

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
var persistenceSet = false

@SuppressLint("SetTextI18n")
class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (!persistenceSet) {
            persistenceSet = true
            FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        }

        setLightDarkModeWallpaper(applicationContext, findViewById<ConstraintLayout>(R.id.LoginActivityConLayout))

        //if the user is already logged in, switch to main activity
        if (mAuth.currentUser != null) {
            val newWindow = Intent(this, MainActivity::class.java)
            startActivity(newWindow)
            finish()
        }

        setupOnClickListeners()
    }
    private fun setupOnClickListeners(){
        //at start make login visible and register invisible
        findViewById<ConstraintLayout>(R.id.registerConLayout).apply {
            alpha = 0f
            visibility = Group.GONE
        }
        findViewById<ConstraintLayout>(R.id.loginConLayout).visibility = Group.VISIBLE

        findViewById<TextView>(R.id.loginButton).setOnClickListener {
            val username = findViewById<EditText>(R.id.loginEmailET).text.toString()
            val password = findViewById<EditText>(R.id.loginPasswordET).text.toString()

            if ((username.isEmpty()) || (password.isEmpty())){
                Toast.makeText(applicationContext, R.string.cantBeEmptyWarning, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else {
                findViewById<ConstraintLayout>(R.id.darkOverlay).apply {
                    visibility = View.VISIBLE
                    alpha = 1f
                    bringToFront()
                    startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.textviewanimation))
                }
                if (mAuth.currentUser != null)
                    FirebaseAuth.getInstance().signOut()
                mAuth.signInWithEmailAndPassword(username, password).addOnSuccessListener {
                    val newWindow = Intent(this, MainActivity::class.java)
                    startActivity(newWindow)
                    finish()
                }.addOnFailureListener {
                    Toast.makeText(applicationContext, R.string.loginFailed, Toast.LENGTH_SHORT).show()
                    findViewById<ConstraintLayout>(R.id.darkOverlay).apply {
                        visibility = View.GONE
                        alpha = 0f
                    }
                }
            }
        }

        findViewById<TextView>(R.id.registerButton).setOnClickListener{
            triggerRegisterMenuVisibility()
        }

        findViewById<TextView>(R.id.registerMainButton).setOnClickListener{
            val stud = Student()

            val email = findViewById<EditText>(R.id.registerEmail).text.toString()
            val password = findViewById<EditText>(R.id.registerPassword).text.toString()

            try {
                stud.name = findViewById<EditText>(R.id.registerUsername).text.toString().trim()
                stud.course = findViewById<EditText>(R.id.registerCourse).text.toString()
                if (findViewById<EditText>(R.id.registerGeneration).text.toString().toInt() != appDefaultNullValue.toInt())
                    stud.gen = findViewById<EditText>(R.id.registerGeneration).text.toString().toInt()
                stud.index = findViewById<EditText>(R.id.registerIndex).text.toString()
            } catch (e: Exception) {
                Toast.makeText(applicationContext, R.string.cantBeEmptyWarning, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            findViewById<ConstraintLayout>(R.id.darkOverlay).apply {
                visibility = View.VISIBLE
                alpha = 1f
                bringToFront()
                startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.textviewanimation))
            }

            mAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener {
                if (mAuth.currentUser != null)
                    FirebaseAuth.getInstance().signOut()
                mAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener {
                    databaseReference.child("StudentDatabase").child(mAuth.uid.toString()).setValue(stud)
                    if (mAuth.currentUser != null) {
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }.addOnFailureListener {
                    findViewById<ConstraintLayout>(R.id.darkOverlay).apply {
                        visibility = View.GONE
                        alpha = 0f
                    }
                }
            }

        }
    }

    private var backPressedTime: Long = 0
    override fun onBackPressed(){
        if (findViewById<ConstraintLayout>(R.id.registerConLayout).visibility == Group.VISIBLE){
            triggerRegisterMenuVisibility()
            return
        }
        if (backPressedTime + 2000 > System.currentTimeMillis()){
            finish()
        }else{
            Toast.makeText(this,R.string.confirmExit, Toast.LENGTH_SHORT).show()
        }
        backPressedTime = System.currentTimeMillis()
    }

    private fun triggerRegisterMenuVisibility(){
        val registerConsLayout = findViewById<ConstraintLayout>(R.id.registerConLayout)
        val loginConsLayout = findViewById<ConstraintLayout>(R.id.loginConLayout)

        when (registerConsLayout.alpha) {
            0f -> {
                registerConsLayout.apply {
                    visibility = View.VISIBLE
                    alpha = 1f
                    bringToFront()
                    startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.menuin))
                }
                loginConsLayout.apply {
                    visibility = View.GONE
                    alpha = 0f
                    startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.menuout))
                }
            }
            else -> {
                loginConsLayout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.menuin))
                loginConsLayout.apply {
                    visibility = View.VISIBLE
                    alpha = 1f
                }
                registerConsLayout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.menuout))
                Handler(Looper.getMainLooper()).postDelayed({
                    registerConsLayout.apply {
                        visibility = View.GONE
                        alpha = 0f
                    }
                }, 120)
            }
        }
    }
}