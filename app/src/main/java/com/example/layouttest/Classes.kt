package com.example.layouttest

import android.content.Context
import android.content.res.Configuration
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder

val defaultCriteriaGradingList = listOf(54, 64, 74, 84, 94)
const val subjectTreeDB = "subjects"
const val studentTreeDB = "StudentDatabase"
const val examTreeDB = "exams"
const val criteriaTreeDB = "criteria"
const val infoTreeDB = "appInfo"
const val appDefaultNullValue = -1.0
const val maxPointsValue = 500
const val databaseSubjectYearOffset = 1900
enum class OptionsEnum{AddExam, EditExam, AddSubject, EditSubject, EditCriteria}
enum class SubjectTreeOption(val tree: String){SubjectProfessor("prof"), SubjectName("name"), SubjectYear("year"), SubjectCode("code"), SubjectNote("note"), SubjectProfessorEmail("profEmail")}
enum class StudentDataTreeOption(val tree: String){StudentName("name"), StudentCourse("course"), StudentGeneration("gen"), StudentIndex("index")}

data class Student(
    var name: String = "",
    var course: String = "",
    var gen: Int = appDefaultNullValue.toInt(),
    var index: String = "",
)

data class Subject(
    var profEmail: String = "",
    var note: String = "",
    var key: String? = "",
    var name: String = "",
    var code: String = "",
    var prof: String = "",
    var year: Int = appDefaultNullValue.toInt(),
    val criteria: MutableList<Int> = defaultCriteriaGradingList as MutableList<Int>
)

data class Exam(
    var key: String? = "",
    var note: String = "",
    var name: String = "",
    var date: String = "",
    var earnedPoints: Double = appDefaultNullValue,
    var maxPoints: Double = appDefaultNullValue,
    var material: String = "",
)

data class AppInfo(
    val minVersion: Double = appDefaultNullValue,
    val maintenance: Boolean = false,
)

fun displayShortToast(ctx: Context,message: String){
    Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show()
}

fun setLightDarkModeWallpaper(ctx: Context, view: View){
    try {
        when (ctx.resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)) {
            Configuration.UI_MODE_NIGHT_YES -> {
                view.background = ContextCompat.getDrawable(ctx, R.drawable.bg3)
            }
            Configuration.UI_MODE_NIGHT_NO -> {
                view.background = ContextCompat.getDrawable(ctx, R.drawable.bg3light)
            }
            Configuration.UI_MODE_NIGHT_UNDEFINED -> {
                view.background = ContextCompat.getDrawable(ctx, R.drawable.bg3light)
            }
        }
    } catch (_: Exception) {}
}

fun showDialog(ctx: Context, title: String = "", body: String = "", callback: () -> Unit){
    try{
        val builder = MaterialAlertDialogBuilder(ctx, R.style.todoDialogLight)
        builder.setTitle(title)
        builder.setMessage(body)

        builder.setPositiveButton("Uredu") { _, _ ->
            callback()
        }

        builder.setCancelable(false)
        builder.show()
    }catch(_: Exception){}
}

//Popraviti tastaturu datum
//Back na mainu ne treba da odjavljuje vec da izlazi
//Tekst naslova i dugmadi pojednostavit
//Stavit dugmadi jedan ispod drugog
//KriteriJ bodovanja, dodaj xy, uredi xy
//Potvrdi, default, odbaci, ukloni dugmad
//Ranku bijeli navigation bar u dark modu, crni u light modu
//Semafor dugme
//Email i materijal link nije vidljive boje
//Dark mode svijetlo plava, Light mode tamno plava
//Default kriteriji crveni, cancel žuti
//Odjava u postavkama, odjavi se
//Pomjeri opcije dugme ulijevo
//Godina mora biti dvocifren broj