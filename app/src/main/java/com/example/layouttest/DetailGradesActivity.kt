package com.example.layouttest

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlin.math.round

@SuppressLint("SetTextI18n")
class DetailGradesActivity : AppCompatActivity() {

    private var totalScore: Double = 0.0
    private var subjectKey: String = ""
    private var loading: Boolean = true
    private val examList = mutableListOf<Exam>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_grades)

        setLightDarkModeWallpaper(applicationContext, findViewById<ConstraintLayout>(R.id.DetailGradesActivityConLayout))

        onClickListenerSetup()
        startInvisibleElements()
        fetchExamBaseData()
        fadeInElements()
    }

    private fun fetchExamBaseData(){
        subjectKey = intent.getStringExtra(R.string.subjectKeyArgument.toString()).toString()

        //reference to data tree
        val subjectListData = rootDBReferenceFirebase.child(subjectTreeDB).child(subjectKey).child(examTreeDB)

        //fetch exam data
        subjectListData.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                examList.clear()

                //iterate through every exam
                val exams = snapshot.children
                exams.forEach { exam ->
                    var newExamEntry = Exam()
                    exam.getValue(Exam::class.java)?.let {
                        newExamEntry = it
                    }
                    newExamEntry.key = exam.key.toString()
                    examList.add(newExamEntry)
                }

                findViewById<TextView>(R.id.emptyExamsListTextView).visibility =
                    if (examList.isEmpty())
                        View.VISIBLE
                    else
                        View.GONE

                //update activity data
                updateActivity()
            }
            override fun onCancelled(error: DatabaseError) {}
        })

        //reference to data tree
        val subjectData = rootDBReferenceFirebase.child(subjectTreeDB).child(subjectKey)
        var sub = Subject()

        //fetch subject data
        subjectData.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.getValue(Subject::class.java)?.let {
                    sub = it
                }

                //UI elements
                val detailsSubjectNameTV: TextView = findViewById(R.id.detailsSubjectNameTV)
                val detailsSubjectCodeTV: TextView = findViewById(R.id.detailsSubjectCodeTV)
                val detailsSubjectProfTV: TextView = findViewById(R.id.detailsSubjectProfTV)
                val detailsSubjectYearTV: TextView = findViewById(R.id.detailsSubjectYearTV)
                val detailsSubjectNoteTV: TextView = findViewById(R.id.detailsSubjectNoteTV)
                val detailsSubjectProfEmailTV: TextView = findViewById(R.id.detailsSubjectProfEmailTV)

                //set the name, code and year
                detailsSubjectNameTV.text = sub.name

                if (sub.code.isNotBlank()) {
                    detailsSubjectCodeTV.visibility = View.VISIBLE
                    findViewById<TextView>(R.id.detailsSubjectCodeDescTV).visibility = View.VISIBLE
                    detailsSubjectCodeTV.text = sub.code
                }else{
                    detailsSubjectCodeTV.visibility = View.GONE
                    findViewById<TextView>(R.id.detailsSubjectCodeDescTV).visibility = View.GONE
                }

                if (sub.year != appDefaultNullValue.toInt()) {
                    detailsSubjectYearTV.text = "${sub.year + databaseSubjectYearOffset}/${sub.year + databaseSubjectYearOffset + 1}"
                }else{
                    detailsSubjectYearTV.visibility = View.GONE
                }

                if (sub.prof.isNotBlank()) {
                    detailsSubjectProfTV.visibility = View.VISIBLE
                    findViewById<TextView>(R.id.detailsSubjectProfDescTV).visibility = View.VISIBLE
                    detailsSubjectProfTV.text = sub.prof
                }else{
                    detailsSubjectProfTV.visibility = View.GONE
                    findViewById<TextView>(R.id.detailsSubjectProfDescTV).visibility = View.GONE
                }

                if (sub.note.isNotBlank()) {
                    detailsSubjectNoteTV.visibility = View.VISIBLE
                    findViewById<TextView>(R.id.detailsSubjectNoteDescTV).visibility = View.VISIBLE
                    detailsSubjectNoteTV.text = sub.note
                }else{
                    detailsSubjectNoteTV.visibility = View.GONE
                    findViewById<TextView>(R.id.detailsSubjectNoteDescTV).visibility = View.GONE
                }

                if (sub.profEmail.isNotBlank()) {
                    detailsSubjectProfEmailTV.visibility = View.VISIBLE
                    findViewById<TextView>(R.id.detailsSubjectProfEmailDescTV).visibility = View.VISIBLE
                    detailsSubjectProfEmailTV.text = sub.profEmail
                }else{
                    detailsSubjectProfEmailTV.visibility = View.GONE
                    findViewById<TextView>(R.id.detailsSubjectProfEmailDescTV).visibility = View.GONE
                }

            }
            override fun onCancelled(error: DatabaseError) {}
        })

    }

    private fun updateActivity() {
        pullGradeCriteria()

        val detailsTotalScoreTextView: TextView = findViewById(R.id.detailsTotalScoreTextView)
        val examListView: ListView = findViewById(R.id.detailsExamListView)

        //adapter for controlling the exam list
        val adapterExamList = AdapterForExam(this, R.layout.examadapterviewlayout, examList)
        examListView.adapter = adapterExamList

        examListView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            examList[position].key?.let {
                launchAddActivity(applicationContext, OptionsEnum.EditExam, subjectKey,
                    it
                )
            }
        }

        //reset previous total
        totalScore = 0.0

        //add up total score
        for (exam in examList) {
            if (exam.earnedPoints >= 0)
                totalScore += exam.earnedPoints
        }

        //accuracy missing sometimes, call round() again just in case
        totalScore = round(totalScore * 100) / 100

        //update
        detailsTotalScoreTextView.text = totalScore.toString()
        adapterExamList.notifyDataSetChanged()
        loading = false
    }

    private fun startInvisibleElements(){
        findViewById<ConstraintLayout>(R.id.detailsSubjectPointsConsLayout).alpha = 0f
        findViewById<ListView>(R.id.detailsExamListView).alpha = 0f
        findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).apply{
            visibility = View.GONE
            alpha = 0f
        }
    }

    private fun fadeInElements(){
        val constraintLayout: View = findViewById(R.id.detailsSubjectPointsConsLayout)
        val examListView: ListView = findViewById(R.id.detailsExamListView)

        //values for animation offset
        val offsetAnim1: Long = 90
        val offsetAnim2: Long = 190

        //animations
        val anim = AnimationUtils.loadAnimation(applicationContext, R.anim.titleviewanimation)
        anim.startOffset = offsetAnim1

        anim.startOffset = offsetAnim1
        constraintLayout.alpha = 1f
        constraintLayout.startAnimation(anim)

        val anim1 = AnimationUtils.loadAnimation(applicationContext, R.anim.titleviewanimation)
        anim1.startOffset = offsetAnim2
        examListView.alpha = 1f
        examListView.startAnimation(anim1)
    }

    @SuppressLint("ResourceAsColor")
    private fun pullGradeCriteria(){
        val subjectCriteriaData = rootDBReferenceFirebase.child(subjectTreeDB).child(subjectKey).child(criteriaTreeDB)
        val criteriaList = mutableListOf<Int>()

        //pull grade criteria data
        subjectCriteriaData.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                criteriaList.clear()

                //fetch criteria
                val items = snapshot.children
                items.forEach { item ->
                    val num = item.value.toString().toInt()
                    criteriaList.add(num)
                }

                criteriaList.sort()

                //after data is fetched, proceed to output it
                updateGradeCriteriaDisplay(criteriaList)
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }

    fun updateGradeCriteriaDisplay(criteriaList: MutableList<Int>){
        val gradeTextView: TextView = findViewById(R.id.detailsCurrentGrade)
        val detailsPointsToNextGrade: TextView = findViewById(R.id.detailsPointsToNextGrade)
        val detailsPointsToNextGradeTextView: TextView = findViewById(R.id.detailsPointsToNextGradeTextView)
        var grade = 5
        var pointsNextGrade = 0.0

        //if DB criteria list is not given, assume default grading system
        if(criteriaList.isEmpty())
            criteriaList.addAll(defaultCriteriaGradingList)

        //calculate grade
        for (item in criteriaList) {
            if (totalScore >= item) {
                grade++
            }else{
                pointsNextGrade = item - totalScore
                pointsNextGrade = round(pointsNextGrade * 100) / 100
                break
            }
        }

        //display points
        gradeTextView.text = grade.toString()
        if (grade == 5)
            gradeTextView.text = getString(R.string.missingGrade)
        detailsPointsToNextGrade.text = pointsNextGrade.toString()

        //text for next grade
        detailsPointsToNextGradeTextView.text = "${getString(R.string.pointsToNext)} ${grade+1}"

        //hide points required for next grade if max grade is reached
        if(grade == 10){
            detailsPointsToNextGrade.visibility = View.GONE
            detailsPointsToNextGradeTextView.visibility = View.GONE
        }else {
            detailsPointsToNextGrade.visibility = View.VISIBLE
            detailsPointsToNextGradeTextView.visibility = View.VISIBLE
        }
    }

    private fun onClickListenerSetup() {
        findViewById<TextView>(R.id.addExamButton).setOnClickListener {
            launchAddActivity(applicationContext, OptionsEnum.AddExam, subjectKey)
        }

        findViewById<ConstraintLayout>(R.id.detailsSubjectPointsConsLayout).setOnClickListener {
            launchAddActivity(applicationContext, OptionsEnum.EditCriteria, subjectKey)
        }

        findViewById<TextView>(R.id.detailsMoreButton).setOnClickListener{
            val fadeIn = AnimationUtils.loadAnimation(this, R.anim.cons_fade_in)
            val fadeOut = AnimationUtils.loadAnimation(this, R.anim.cons_fade_out)

            if (findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).visibility == View.VISIBLE) {
                findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).startAnimation(fadeOut)

                Handler(Looper.getMainLooper()).postDelayed({
                    findViewById<ConstraintLayout>(R.id.detailsSubjectPointsConsLayout).startAnimation(fadeIn)
                    findViewById<ListView>(R.id.detailsExamListView).startAnimation(fadeIn)

                    findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).apply{
                        visibility = View.GONE
                        alpha = 0f
                    }

                    findViewById<ConstraintLayout>(R.id.detailsSubjectPointsConsLayout).apply {
                        visibility = View.VISIBLE
                        startAnimation(fadeIn)
                        alpha = 1f
                    }
                    findViewById<ListView>(R.id.detailsExamListView).apply {
                        visibility = View.VISIBLE
                        startAnimation(fadeIn)
                        alpha = 1f
                    }
                }, 100)
            }else{
                findViewById<ConstraintLayout>(R.id.detailsSubjectPointsConsLayout).startAnimation(fadeOut)
                findViewById<ListView>(R.id.detailsExamListView).startAnimation(fadeOut)

                Handler(Looper.getMainLooper()).postDelayed({
                    findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).apply {
                        visibility = View.VISIBLE
                        startAnimation(fadeIn)
                        alpha = 1f
                    }

                    findViewById<ConstraintLayout>(R.id.detailsSubjectPointsConsLayout).visibility = View.GONE
                    findViewById<ListView>(R.id.detailsExamListView).visibility = View.GONE
                }, 100)
            }

            findViewById<TextView>(R.id.detailsSubjectProfEmailTV).setOnClickListener {
                var konsultacijeString = "Poštovanje,\njavljam Vam se za termin uvida iz predmeta ${findViewById<TextView>(R.id.detailsSubjectNameTV).text}.\n\nSrdačan pozdrav"
                rootDBReferenceFirebase.get().addOnSuccessListener { snapshot ->
                    snapshot.getValue(Student::class.java)?.let {
                        konsultacijeString = "$konsultacijeString\n${it.name}"

                        if (it.index.isNotBlank())
                            konsultacijeString = "$konsultacijeString\n${it.index}"
                    }
                }

                AlertDialog.Builder(this, R.style.todoDialogLight)
                    .setTitle("Potvrda")
                    .setMessage("Generisati email za uvid?")
                    .setPositiveButton("Da") { _, _ ->
                        sendEmail(findViewById<TextView>(R.id.detailsSubjectProfEmailTV).text.toString(), "Upit ${findViewById<TextView>(R.id.detailsSubjectNameTV).text}", konsultacijeString)
                    }
                    .setNegativeButton("Ne") { _, _ ->
                    }
                    .setCancelable(false)
                    .show()
            }

            findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).setOnClickListener {
                launchAddActivity(applicationContext, OptionsEnum.EditSubject, subjectKey)
            }
        }

    }

    private fun sendEmail(receiverEmail: String, subject: String, body: String){
        val intent = Intent(Intent.ACTION_VIEW)
        val data: Uri = Uri.parse(
            "mailto:$receiverEmail?subject=" + Uri.encode(subject).toString() + "&body=" + Uri.encode(body)
        )
        intent.data = data
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).visibility == View.GONE)
            super.onBackPressed()
        else {
            val fadeIn = AnimationUtils.loadAnimation(this, R.anim.cons_fade_in)
            val fadeOut = AnimationUtils.loadAnimation(this, R.anim.cons_fade_out)

            findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).startAnimation(fadeOut)

            Handler(Looper.getMainLooper()).postDelayed({
                findViewById<ConstraintLayout>(R.id.detailsSubjectPointsConsLayout).startAnimation(
                    fadeIn
                )
                findViewById<ListView>(R.id.detailsExamListView).startAnimation(fadeIn)

                findViewById<ConstraintLayout>(R.id.detailsSubjectDetailsConsLayout).apply {
                    visibility = View.GONE
                    alpha = 0f
                }

                findViewById<ConstraintLayout>(R.id.detailsSubjectPointsConsLayout).apply {
                    visibility = View.VISIBLE
                    startAnimation(fadeIn)
                    alpha = 1f
                }
                findViewById<ListView>(R.id.detailsExamListView).apply {
                    visibility = View.VISIBLE
                    startAnimation(fadeIn)
                    alpha = 1f
                }
            }, 100)
        }
    }

}