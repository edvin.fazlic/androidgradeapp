package com.example.layouttest

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.pm.PackageInfo
import android.os.*
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

val databaseReference = FirebaseDatabase.getInstance().reference
var student = Student()
val rootDBReferenceFirebase = databaseReference.child(studentTreeDB).child(mAuth.uid.toString())

@SuppressLint("SetTextI18n")
class MainActivity : AppCompatActivity() {

    //return to login screen if the main screen opens for any reason without authentication
    private fun kickNonAuthUser() {
        if (FirebaseAuth.getInstance().currentUser == null){
            val newActivity = Intent(this, LoginActivity::class.java)
            startActivity(newActivity)
            finish()
        }
    }

    @Suppress("DEPRECATION")
    private fun checkServerRequirements(){
        val dbReference = FirebaseDatabase.getInstance().reference
        var infoObject = AppInfo()

        dbReference.child(infoTreeDB).addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.getValue(AppInfo::class.java)?.let { info ->
                    infoObject = info
                }

                val pInfo: PackageInfo = applicationContext.packageManager.getPackageInfo(applicationContext.packageName, 0)
                val versionCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    pInfo.longVersionCode.toInt()
                } else {
                    pInfo.versionCode
                }

                if (infoObject.maintenance) {
                    showDialog(this@MainActivity, "Usluga nedostupna", "Server ne radi zbog održavanja", (::finishAndRemoveTask))
                    return
                }

                if (versionCode < infoObject.minVersion) {
                    showDialog(this@MainActivity, "Nadogradnja", "Morate nadograditi verziju aplikacije", (::finishAndRemoveTask))
                    return
                }

                continueSetup()
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }
/*
    fun showDialog(title: String = "", body: String = ""){
        try{
            val builder = MaterialAlertDialogBuilder(this, R.style.todoDialogLight)
            builder.setTitle(title)
            builder.setMessage(body)

            builder.setPositiveButton("Uredu") { _, _ ->
                finishAndRemoveTask()
            }

            builder.setCancelable(false)
            builder.show()
        }catch(_: Exception){}
    }*/

    //list of subject codes to pass to Detail grades activity
    private val subjectCodes = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<TextView>(R.id.loadingTextView).visibility = View.VISIBLE

        FirebaseAuth.getInstance().addAuthStateListener {
            if (it.currentUser == null){
                kickNonAuthUser()
            }
        }

        setLightDarkModeWallpaper(applicationContext, findViewById<ConstraintLayout>(R.id.MainActivityConLayout))

        //check version and maintenance status
        checkServerRequirements()
    }

    private fun continueSetup(){
        //pull data for current student
        pullUserData()

        //make all elements invisible, later make needed ones visible
        startInvisible()

        //setup menu
        optionsMenuSetup()

        setupSubjectList()
    }

    private fun pullUserData() {
        val textViewUsername: TextView = findViewById(R.id.TextViewUsername)
        val textViewYear: TextView = findViewById(R.id.TextViewYear)
        val textViewCollegeCourse: TextView = findViewById(R.id.TextViewCollegeCourse)

        rootDBReferenceFirebase.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.getValue(Student::class.java)?.let {
                    student = it
                }

                if (student.gen != appDefaultNullValue.toInt()) {
                    textViewYear.visibility = View.VISIBLE
                    student.gen += databaseSubjectYearOffset
                    textViewYear.text = "Generacija ${student.gen}/${student.gen+1}"
                }else{
                    textViewYear.visibility = View.GONE
                }

                textViewCollegeCourse.visibility = View.VISIBLE
                textViewCollegeCourse.text = "${student.course} ${student.index}"

                if(textViewCollegeCourse.text.isBlank())
                    textViewCollegeCourse.height = 0
                else
                    textViewCollegeCourse.height = 60

                textViewUsername.text = student.name

                //update subject list
                setupSubjectList()
                startActivityEntryAnimation()
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }

    private fun startInvisible(){
        findViewById<TextView>(R.id.TextViewUsername).alpha = 0f
        findViewById<TextView>(R.id.TextViewYear).visibility = View.GONE
        findViewById<TextView>(R.id.TextViewCollegeCourse).visibility = View.GONE
        findViewById<ListView>(R.id.listViewSubjects).alpha = 0f
        findViewById<View>(R.id.mainTitleConsLayout).alpha = 0f
        //make the loading label visible at the start
    }

    private fun startActivityEntryAnimation() {
        val buttonEdit: TextView = findViewById(R.id.mainMoreButton)
        val buttonAdd: TextView = findViewById(R.id.addTextView)
        val textViewUsername: TextView = findViewById(R.id.TextViewUsername)
        val textViewYear: TextView = findViewById(R.id.TextViewYear)
        val textViewCollegeCourse: TextView = findViewById(R.id.TextViewCollegeCourse)
        val textViewLoading: TextView = findViewById(R.id.loadingTextView)
        val listViewSubjects: ListView = findViewById(R.id.listViewSubjects)
        val consLayoutTop: View = findViewById(R.id.mainTitleConsLayout)

        val offsetAnim1: Long = 60
        val offsetAnim2: Long = 200
        val offsetAnim3: Long = 260
        val offsetAnim4: Long = 320

        textViewLoading.visibility = View.GONE

        val anim0 = AnimationUtils.loadAnimation(this, R.anim.textviewanimation)
        anim0.startOffset = 0

        val anim1 = AnimationUtils.loadAnimation(this, R.anim.titleviewanimation)
        anim1.startOffset = offsetAnim1

        val anim2 = AnimationUtils.loadAnimation(this, R.anim.titleviewanimation)
        anim2.startOffset = offsetAnim2

        val anim3 = AnimationUtils.loadAnimation(this, R.anim.titleviewanimation)
        anim3.startOffset = offsetAnim3

        val anim4 = AnimationUtils.loadAnimation(this, R.anim.titleviewanimation)
        anim4.startOffset = offsetAnim4

        buttonEdit.apply {
            alpha = 1f
            startAnimation(anim0)
        }

        buttonAdd.apply {
            alpha = 1f
            startAnimation(anim0)
        }

        consLayoutTop.apply {
            alpha = 1f
            startAnimation(anim0)
        }

        textViewUsername.apply {
            alpha = 1f
            startAnimation(anim1)
        }

        textViewYear.apply {
            alpha = 1f
            startAnimation(anim2)
        }

        textViewCollegeCourse.apply {
            alpha = 1f
            startAnimation(anim2)
        }

        listViewSubjects.apply {
            alpha = 1f
            startAnimation(anim4)
        }

    }

    //back button on request no longer logs you out
    /*//logout if back button is pressed more than once within 2 seconds
    private var backPressedTime: Long = 0
    override fun onBackPressed(){
        val logoutTime = 2000
        if (backPressedTime + logoutTime > System.currentTimeMillis()){
            mAuth.signOut()
            kickNonAuthUser()
        }else {
            Toast.makeText(this,getString(R.string.confirmLogout), Toast.LENGTH_SHORT).show()
        }
        backPressedTime = System.currentTimeMillis()
    }*/

    private fun setupSubjectList(){
        //references to data trees
        val studentSubjectData = rootDBReferenceFirebase.child(subjectTreeDB)

        //list of subjects that need to be fetched from DB
        val listOfSubjectNames = mutableListOf<String>()

        //subjects objects fetched from DB
        val listOfSubjectVariables = mutableListOf<Subject>()

        //listview of subjects UI
        val subList: ListView = findViewById(R.id.listViewSubjects)

        //adapter for subject list view
        val adapter = AdapterForSubject(this, R.layout.subjectsadapterviewlayout, listOfSubjectVariables)
        subList.adapter = adapter

        //start with element being invisible
        subList.alpha = 0f

        //fetch list od subjects
        studentSubjectData.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                listOfSubjectNames.clear()
                listOfSubjectVariables.clear()
                subjectCodes.clear()

                //iterate through exam list
                val subjects = snapshot.children
                subjects.forEach { subject ->
                    var sub = Subject()
                    subject.getValue(Subject::class.java)?.let {
                        sub = it
                    }
                    subjectCodes.add(subject.key.toString())
                    listOfSubjectVariables.add(sub)
                }

                if (listOfSubjectVariables.isEmpty())
                    findViewById<TextView>(R.id.emptySubjectListTextView).visibility = View.VISIBLE
                else
                    findViewById<TextView>(R.id.emptySubjectListTextView).visibility = View.GONE

                //update activity with new information
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {}
        })

        //pass subject code to detailGradesActivity for display
        subList.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            Intent(this, DetailGradesActivity::class.java).apply {
                putExtra(R.string.subjectKeyArgument.toString(), subjectCodes[position])
                startActivity(this)
            }
        }

        subList.setOnItemLongClickListener { _, _, position, _ ->
            launchAddActivity(applicationContext, OptionsEnum.EditSubject, subjectCodes[position])
            true
        }
    }

    private fun optionsMenuSetup() {
        findViewById<TextView>(R.id.addTextView).setOnClickListener {
            launchAddActivity(applicationContext, OptionsEnum.AddSubject)
        }

        findViewById<TextView>(R.id.mainMoreButton).setOnClickListener {
            val newActivity = Intent(applicationContext, SettingsActivity::class.java)
            startActivity(newActivity)
        }
    }

}

fun launchAddActivity(context: Context, options: OptionsEnum, subjectCode: String = "", examCode: String = ""){
    val newActivity = Intent(context, AddActivity::class.java)
    newActivity.addFlags(FLAG_ACTIVITY_NEW_TASK)

    when (options){
        OptionsEnum.EditSubject, OptionsEnum.EditCriteria, OptionsEnum.AddExam -> {
            newActivity.putExtra(R.string.optionsEnumArgument.toString(), options)
            newActivity.putExtra(R.string.subjectKeyArgument.toString(), subjectCode)
        }
        OptionsEnum.EditExam -> {
            newActivity.putExtra(R.string.optionsEnumArgument.toString(), options)
            newActivity.putExtra(R.string.subjectKeyArgument.toString(), subjectCode)
            newActivity.putExtra(R.string.examKeyArgument.toString(), examCode)
        }
        OptionsEnum.AddSubject -> {
            newActivity.putExtra(R.string.optionsEnumArgument.toString(), OptionsEnum.AddSubject)
        }
    }

    context.startActivity(newActivity)
}


