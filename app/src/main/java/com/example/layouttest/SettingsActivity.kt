package com.example.layouttest

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.firebase.auth.FirebaseAuth
class SettingsActivity : AppCompatActivity() {

    private lateinit var settingsNameET: EditText
    private lateinit var settingsCourseET: EditText
    private lateinit var settingsGenerationET: EditText
    private lateinit var settingsIndexET: EditText
    private lateinit var settingsMainButton: TextView
    private lateinit var settingsCancelButton: TextView
    private lateinit var settingsLogoutButton: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        settingsNameET = findViewById(R.id.settingsNameET)
        settingsGenerationET = findViewById(R.id.settingsGenerationET)
        settingsCourseET = findViewById(R.id.settingsCourseET)
        settingsIndexET = findViewById(R.id.settingsIndexET)
        settingsMainButton = findViewById(R.id.settingsMainButton)
        settingsCancelButton = findViewById(R.id.settingsCancelButton)
        settingsLogoutButton = findViewById(R.id.settingsLogoutButton)

        setLightDarkModeWallpaper(applicationContext, findViewById<ConstraintLayout>(R.id.SettingsActivityConsLayout))

        pullData()
        setupOnClickListeners()
    }
    private fun setupOnClickListeners(){
        settingsCancelButton.setOnClickListener {
            onBackPressed()
        }

        settingsLogoutButton.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            finish()
        }

        settingsMainButton.setOnClickListener {
            checkEligibilitySettings()?.let{ message ->
                displayShortToast(applicationContext, message)
                return@setOnClickListener
            }

            updateData()
            onBackPressed()
        }
    }
    @SuppressLint("SetTextI18n")
    private fun pullData(){
        rootDBReferenceFirebase.get().addOnSuccessListener { snapshot ->
            snapshot.getValue(Student::class.java)?.let {student ->
                settingsNameET.setText(student.name)
                settingsCourseET.setText(student.course)
                settingsIndexET.setText(student.index)
                settingsGenerationET.setText("${student.gen + databaseSubjectYearOffset}")
            }
        }
    }
    private fun checkEligibilitySettings(): String?{
        if (settingsNameET.text.isBlank())
            return "Ime studenta nije validno"

        return null
    }
    private fun updateData(){
        //Student data section
        val ref = rootDBReferenceFirebase
        ref.child(StudentDataTreeOption.StudentName.tree).setValue(settingsNameET.text.toString())
        ref.child(StudentDataTreeOption.StudentCourse.tree).setValue(settingsCourseET.text.toString())
        ref.child(StudentDataTreeOption.StudentIndex.tree).setValue(settingsIndexET.text.toString())
        ref.child(StudentDataTreeOption.StudentGeneration.tree).setValue((settingsGenerationET.text.toString().trim()).toInt() - databaseSubjectYearOffset)
    }
}