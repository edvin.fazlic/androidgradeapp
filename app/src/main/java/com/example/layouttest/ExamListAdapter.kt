package com.example.layouttest

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

@SuppressLint("SetTextI18n")
class AdapterForExam(private var mCtx: Context, private var resources: Int, private var items: List<Exam>):
    ArrayAdapter<Exam>(mCtx, resources, items) {

    @SuppressLint("ViewHolder", "SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resources, null)

        val examName: TextView = view.findViewById(R.id.examTitle)
        val examCode: TextView = view.findViewById(R.id.examDate)
        val examMaterial: TextView = view.findViewById(R.id.examMaterial)
        val examMaterialTitle: TextView = view.findViewById(R.id.examMaterialTitle)
        val examPointsEarned: TextView = view.findViewById(R.id.examPointsEarned)
        val examNoteTitle: TextView = view.findViewById(R.id.examNoteTitle)
        val examNote: TextView = view.findViewById(R.id.examNote)

        val mItem: Exam = items[position]

        examName.text = mItem.name
        examCode.text = mItem.date

        var earnedString = "-"
        var maxString = "-"

        if (mItem.maxPoints >= 0){
            maxString = mItem.maxPoints.toString()
        }

        if (mItem.earnedPoints >= 0){
            earnedString = mItem.earnedPoints.toString()
        }

        examPointsEarned.text = "$earnedString / $maxString"

        if(mItem.material == ""){
            examMaterial.visibility = View.GONE
            examMaterialTitle.visibility = View.GONE
        }else{
            examMaterial.text = mItem.material
        }

        if(mItem.note == ""){
            examNoteTitle.visibility = View.GONE
            examNote.visibility = View.GONE
        }else{
            examNote.text = mItem.note
        }

        return view
    }

}