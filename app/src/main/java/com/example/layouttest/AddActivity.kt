package com.example.layouttest

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.util.*

@SuppressLint("SetTextI18n")
class AddActivity : AppCompatActivity() {

    private val subjectNameList = mutableListOf<String>()
    private val subjectList = mutableListOf<Subject>()
    private val listOfExistingExams = mutableListOf<Exam>()
    private lateinit var subjectAdapter: ArrayAdapter<String>
    private var option: OptionsEnum? = null
    private var subCode = ""
    private var examCode = ""
    private var examObject = Exam()

    private lateinit var subjectMainButton: TextView
    private lateinit var examMainButton: TextView
    private lateinit var criteriaMainButton: TextView

    private lateinit var subjectRemoveButton: TextView
    private lateinit var examRemoveButton: TextView

    private lateinit var subjectCancelButton: TextView
    private lateinit var examCancelButton: TextView
    private lateinit var criteriaCancelButton: TextView
    private lateinit var criteriaDefaultCriteriaButton: TextView

    private lateinit var subjectScrollView: ScrollView
    private lateinit var examScrollView: ScrollView
    private lateinit var criteriaScrollView: ScrollView

    private lateinit var criteria1ET: EditText
    private lateinit var criteria2ET: EditText
    private lateinit var criteria3ET: EditText
    private lateinit var criteria4ET: EditText
    private lateinit var criteria5ET: EditText

    private lateinit var examNameET: EditText
    private lateinit var examDateET: EditText
    private lateinit var examPointsET: EditText
    private lateinit var examMaxPointsET: EditText
    private lateinit var examMaterialET: EditText
    private lateinit var examNoteET: EditText

    private lateinit var subjectNameET: EditText
    private lateinit var subjectCodeET: EditText
    private lateinit var subjectYearET: EditText
    private lateinit var subjectProfET: EditText
    private lateinit var subjectNoteET: EditText
    private lateinit var subjectProfEmailET: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

        setupInterface()
    }

    private fun setupInterface(){
        subjectMainButton = findViewById(R.id.subjectMainButton)
        examMainButton = findViewById(R.id.examMainButton)
        criteriaMainButton = findViewById(R.id.criteriaMainButton)

        subjectRemoveButton = findViewById(R.id.subjectRemoveButton)
        examRemoveButton = findViewById(R.id.examRemoveButton)

        subjectCancelButton = findViewById(R.id.subjectCancelButton)
        examCancelButton = findViewById(R.id.examCancelButton)
        criteriaCancelButton = findViewById(R.id.criteriaCancelButton)
        criteriaDefaultCriteriaButton = findViewById(R.id.criteriaDefaultCriteriaButton)

        subjectScrollView = findViewById(R.id.subjectScrollView)
        examScrollView = findViewById(R.id.examScrollView)
        criteriaScrollView = findViewById(R.id.criteriaScrollView)

        examNameET = findViewById(R.id.examNameET)
        examDateET = findViewById(R.id.examDateET)
        examPointsET = findViewById(R.id.examPointsET)
        examMaxPointsET = findViewById(R.id.examMaxPointsET)
        examMaterialET = findViewById(R.id.examMaterialET)
        examNoteET = findViewById(R.id.examNoteET)

        subjectNameET = findViewById(R.id.subjectNameET)
        subjectCodeET = findViewById(R.id.subjectCodeET)
        subjectYearET = findViewById(R.id.subjectYearET)
        subjectProfET = findViewById(R.id.subjectProfET)
        subjectNoteET = findViewById(R.id.subjectNoteET)
        subjectProfEmailET = findViewById(R.id.subjectProfEmailET)

        criteria1ET = findViewById(R.id.criteria1ET)
        criteria2ET = findViewById(R.id.criteria2ET)
        criteria3ET = findViewById(R.id.criteria3ET)
        criteria4ET = findViewById(R.id.criteria4ET)
        criteria5ET = findViewById(R.id.criteria5ET)

        //light/dark theme setup
        setLightDarkModeWallpaper(applicationContext, findViewById<ConstraintLayout>(R.id.AddActivityConLayout))

        //setup array adapter for the spinners
        subjectAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, subjectNameList)

        //get intent extra from main activity
        intent.getSerializableExtra(R.string.optionsEnumArgument.toString())?.let {
            option = it as OptionsEnum
        }

        intent.getStringExtra(R.string.subjectKeyArgument.toString())?.let {
            subCode = it
        }

        intent.getStringExtra(R.string.examKeyArgument.toString())?.let {
            examCode = it
        }

        //set both layout to GONE, later make needed one visible
        examScrollView.visibility = View.GONE
        subjectScrollView.visibility = View.GONE
        criteriaScrollView.visibility = View.GONE

        //first set all optional elements invisible, later make only required ones visible
        subjectRemoveButton.visibility = Group.GONE
        examRemoveButton.visibility = Group.GONE

        val titleTextView = findViewById<TextView>(R.id.addTitleTV)

        when (option){
            OptionsEnum.AddSubject -> {
                setupSubjectOnClickListeners()
                subjectScrollView.visibility = Group.VISIBLE
                titleTextView.text = getString(R.string.addSubjectTitle)
            }
            OptionsEnum.AddExam -> {
                setupExamOnClickListeners()
                examScrollView.visibility = Group.VISIBLE
                titleTextView.text = getString(R.string.addExamitle)
            }
            OptionsEnum.EditExam -> {
                setupExamOnClickListeners()
                examRemoveButton.visibility = Group.VISIBLE
                examScrollView.visibility = Group.VISIBLE
                titleTextView.text = getString(R.string.editExamTitle)
            }
            OptionsEnum.EditSubject -> {
                setupSubjectOnClickListeners()
                subjectRemoveButton.visibility = Group.VISIBLE
                subjectScrollView.visibility = Group.VISIBLE
                titleTextView.text = getString(R.string.editSubjectTitle)
            }
            OptionsEnum.EditCriteria -> {
                setupCriteriaOnClickListeners()
                criteriaScrollView.visibility = Group.VISIBLE
                titleTextView.text = getString(R.string.editCriteriaTitle)
            }
            else -> onBackPressed()
        }
    }

    //setup up all the onClickListeners for the items in the interface
    private fun setupExamOnClickListeners(){
        rootDBReferenceFirebase.child(subjectTreeDB).child(subCode).child(examTreeDB).child(examCode).get().addOnSuccessListener { snapshot ->
            snapshot.getValue(Exam::class.java)?.let {
                examObject = it
            }

            //we want to enable Edit Text when we want to add new exams
            //if we want to edit exams, check whether there are any to edit
            if (examCode.isBlank() && option == OptionsEnum.EditExam)
                setEnabledDataExam(false)

            try{
                displayDataExam(examObject)
                setEnabledDataExam(true)
            } catch (_: Exception) {}
        }

        //list of existing exams to compare later when checking eligibility
        rootDBReferenceFirebase.child(subjectTreeDB).child(subCode).child(examTreeDB).addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {}
            override fun onDataChange(snapshot: DataSnapshot) {
                val exams = snapshot.children
                for (exam in exams) {
                    exam.getValue(Exam::class.java)?.let {
                        listOfExistingExams.add(it)
                    }
                }
            }
        })

        //main button functionality
        examMainButton.setOnClickListener {
            val examObject = getExamObject() ?: return@setOnClickListener

            //check eligibility, if problems are found, display them and abort push to database
            checkEligibilityExam(examObject)?.let { message ->
                displayShortToast(applicationContext, message)
                return@setOnClickListener
            }

            if (option == OptionsEnum.AddExam) {
                pushExamToDB(examObject)
            } else if (option == OptionsEnum.EditExam) {
                updateExamOnDB(examCode, examObject)
            }
            onBackPressed()
        }

        //remove button functionality
        examRemoveButton.setOnClickListener {
            removeExamFromDB(examCode)
        }

        examCancelButton.setOnClickListener {
            onBackPressed()
        }
    }
    private fun setupSubjectOnClickListeners(){
        var subjectObject: Subject

        if (option == OptionsEnum.EditSubject)
            rootDBReferenceFirebase.child(subjectTreeDB).child(subCode).get().addOnSuccessListener { snapshot ->
                snapshot.getValue(Subject::class.java)?.let {
                    subjectObject = it

                    displayDataSubject(subjectObject)
                    setEnabledDataSubject(true)
                }
            }

        if (option == OptionsEnum.AddSubject)
            subjectYearET.setText(Calendar.getInstance().get(Calendar.YEAR).toString())

        //main button functionality
        subjectMainButton.setOnClickListener {
            val sub = getSubjectObject()

            //check eligibility, if problems are found, display them and abort push to database
            checkEligibilitySubject(sub)?.let { message ->
                displayShortToast(applicationContext, message)
                return@setOnClickListener
            }

            if (option == OptionsEnum.AddSubject) {
                pushSubjectToDB(sub)
            } else if (option == OptionsEnum.EditSubject) {
                updateSubjectOnDB(subCode, sub)
            }
            onBackPressed()
        }

        //remove button functionality
        subjectRemoveButton.setOnClickListener {
            removeSubjectFromDB(subCode)
        }

        subjectCancelButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupCriteriaOnClickListeners(){
        val et = mutableListOf<Int>()
        rootDBReferenceFirebase.child(subjectTreeDB).child(subCode).child(criteriaTreeDB).get().addOnSuccessListener { tree ->
            tree?.let { snapshot ->
                snapshot.children.forEach { child ->
                    child.getValue(Int::class.java)?.let { value ->
                        et.add(value)
                    }
                }
                displayDataCriteria(et)
            }
        }

        //main button functionality
        criteriaMainButton.setOnClickListener {
            val criteria = getCriteriaObject()

            //check eligibility, if problems are found, display them and abort push to database
            checkEligibilityCriteria(criteria)?.let { message ->
                displayShortToast(applicationContext, message)
                return@setOnClickListener
            }

            pushCriteriaToDB(subCode, criteria)
            onBackPressed()
        }

        //get default criteria button functionality
        criteriaDefaultCriteriaButton.setOnClickListener {
            val criteria = defaultCriteriaGradingList

            //check eligibility, if problems are found, display them and abort push to database
            checkEligibilityCriteria(criteria)?.let { message ->
                displayShortToast(applicationContext, message)
                return@setOnClickListener
            }

            displayDataCriteria(criteria)
        }

        criteriaCancelButton.setOnClickListener {
            onBackPressed()
        }
    }

    //Clear all the Edit Texts in the subject/exam interface
    private fun clearDataExam(){
        examNameET.setText("")
        examDateET.setText("")
        examPointsET.setText("")
        examMaxPointsET.setText("")
        examNoteET.setText("")
        examMaterialET.setText("")
    }
    private fun clearDataSubject(){
        subjectNameET.setText("")
        subjectCodeET.setText("")
        subjectYearET.setText("")
        subjectProfET.setText("")
        subjectProfEmailET.setText("")
        subjectNoteET.setText("")
    }

    //Enable/disable Edit Texts and Buttons in the subject/exam interface
    private fun setEnabledDataExam(status: Boolean){
        examNameET.isEnabled = status
        examDateET.isEnabled = status
        examPointsET.isEnabled = status
        examMaxPointsET.isEnabled = status
        examNoteET.isEnabled = status
        examMaterialET.isEnabled = status
        examMainButton.isEnabled = status
        examRemoveButton.isEnabled = status
    }
    private fun setEnabledDataSubject(status: Boolean){
        subjectNameET.isEnabled = status
        subjectCodeET.isEnabled = status
        subjectYearET.isEnabled = status
        subjectProfET.isEnabled = status
        subjectProfEmailET.isEnabled = status
        subjectNoteET.isEnabled = status
        subjectMainButton.isEnabled = status
        subjectRemoveButton.isEnabled = status
    }

    //Display the data of the passed subject/exam object in the Edit Texts of the subject/exam interface
    private fun displayDataSubject(sub: Subject){
        clearDataSubject()

        subjectNameET.setText(sub.name)
        subjectCodeET.setText(sub.code)
        subjectProfET.setText(sub.prof)
        subjectNoteET.setText(sub.note)
        subjectProfEmailET.setText(sub.profEmail)
        if (sub.year != appDefaultNullValue.toInt())
            subjectYearET.setText((sub.year + databaseSubjectYearOffset).toString())
    }
    private fun displayDataExam(ex: Exam){
        clearDataExam()

        examNameET.setText(ex.name)
        examDateET.setText(ex.date)
        examNoteET.setText(ex.note)
        examMaterialET.setText(ex.material)

        if (ex.earnedPoints != appDefaultNullValue)
            examPointsET.setText(ex.earnedPoints.toString())
        if (ex.maxPoints != appDefaultNullValue)
            examMaxPointsET.setText(ex.maxPoints.toString())
    }
    private fun displayDataCriteria(sub: List<Int>){
        if (sub.size == 5) {
            criteria1ET.setText(sub[0].toString())
            criteria2ET.setText(sub[1].toString())
            criteria3ET.setText(sub[2].toString())
            criteria4ET.setText(sub[3].toString())
            criteria5ET.setText(sub[4].toString())
        }
    }

    //add/update/delete exam from database
    private fun pushExamToDB(newExamEntry: Exam){
        newExamEntry.key = null
        rootDBReferenceFirebase.child(subjectTreeDB).child(subCode).child(examTreeDB).push().setValue(newExamEntry).addOnCompleteListener {
            displayShortToast(applicationContext, "Ispit dodan")
        }.addOnCanceledListener {
            displayShortToast(applicationContext, R.string.actionFailed.toString())
        }
    }
    private fun updateExamOnDB(examCode: String, examEntry: Exam){
        examEntry.key = null
        val ref = rootDBReferenceFirebase.child(subjectTreeDB).child(subCode).child(examTreeDB).child(examCode)
        ref.setValue(examEntry).addOnCompleteListener {
            displayShortToast(applicationContext, "Ispis uređen")
        }.addOnCanceledListener {
            displayShortToast(applicationContext, R.string.actionFailed.toString())
        }
    }
    private fun removeExamFromDB(examCode: String){
        val ref = rootDBReferenceFirebase.child(subjectTreeDB).child(subCode).child(examTreeDB).child(examCode)

        val builder = AlertDialog.Builder(this, R.style.todoDialogLight)
        builder.setTitle("Potvrda")
        builder.setMessage("Da li ste sigurni da želite obrisati ispit?")

        builder.setPositiveButton("Obriši") { _, _ ->
            ref.removeValue().addOnCompleteListener {
                displayShortToast(applicationContext, "Ispit uklonjen")
            }.addOnCanceledListener {
                displayShortToast(applicationContext, R.string.actionFailed.toString())
            }
            onBackPressed()
        }

        builder.setNegativeButton("Otkaži") { _, _ ->
        }

        builder.show()
    }

    //add/update/delete subject from database
    private fun pushSubjectToDB(newSubjectEntry: Subject){
        newSubjectEntry.key = null
        rootDBReferenceFirebase.child(subjectTreeDB).push().setValue(newSubjectEntry).addOnCompleteListener {
            displayShortToast(applicationContext, "Predmet dodan")
        }.addOnCanceledListener {
            displayShortToast(applicationContext, R.string.actionFailed.toString())
        }
    }
    private fun updateSubjectOnDB(subjectCode: String, subjectEntry: Subject){
        subjectEntry.key = null
        val ref = rootDBReferenceFirebase.child(subjectTreeDB).child(subjectCode)
        ref.child(SubjectTreeOption.SubjectName.tree).setValue(subjectEntry.name)
        ref.child(SubjectTreeOption.SubjectProfessor.tree).setValue(subjectEntry.prof)
        ref.child(SubjectTreeOption.SubjectYear.tree).setValue(subjectEntry.year)
        ref.child(SubjectTreeOption.SubjectCode.tree).setValue(subjectEntry.code)
        ref.child(SubjectTreeOption.SubjectNote.tree).setValue(subjectEntry.note)
        ref.child(SubjectTreeOption.SubjectProfessorEmail.tree).setValue(subjectEntry.profEmail)
    }
    private fun removeSubjectFromDB(subjectCode: String){
        val ref = rootDBReferenceFirebase.child(subjectTreeDB).child(subjectCode)

        val builder = AlertDialog.Builder(this, R.style.todoDialogLight)
        builder.setTitle("Potvrda")
        builder.setMessage("Da li ste sigurni da želite predmet?")

        builder.setPositiveButton("Obriši") { _, _ ->
            ref.removeValue().addOnCompleteListener {
                displayShortToast(applicationContext, "Predmet uklonjen")
            }.addOnCanceledListener {
                displayShortToast(applicationContext, R.string.actionFailed.toString())
            }
            onBackPressed()
        }

        builder.setNegativeButton("Otkaži") { _, _ ->
        }

        builder.show()
    }

    //collect data from the Edit Texts and return an subject/exam object containing that data
    private fun getSubjectObject(): Subject {
        val newSubjectEntry = Subject()

        newSubjectEntry.name = subjectNameET.text.toString().trim()
        newSubjectEntry.code = subjectCodeET.text.toString().trim()
        if (subjectYearET.text.isNotEmpty()) {
            newSubjectEntry.year = subjectYearET.text.toString().trim().toInt() - databaseSubjectYearOffset
        }
        newSubjectEntry.prof = subjectProfET.text.toString().trim()
        newSubjectEntry.note = subjectNoteET.text.toString().trim()
        newSubjectEntry.profEmail = subjectProfEmailET.text.toString().trim()

        return newSubjectEntry
    }
    private fun getExamObject(): Exam? {
        val newExamEntry = Exam()

        newExamEntry.name = examNameET.text.toString().trim()
        newExamEntry.date = examDateET.text.toString().trim()

        try{
            if (examPointsET.text.toString().isNotEmpty())
                newExamEntry.earnedPoints = examPointsET.text.toString().toDouble()
        }catch (_: Exception){
            displayShortToast(applicationContext, "Unos osvojenih bodova nije validan")
            return null;
        }
        try{
            if (examMaxPointsET.text.toString().isNotEmpty())
                newExamEntry.maxPoints = examMaxPointsET.text.toString().toDouble()
        }catch (_: Exception){
            displayShortToast(applicationContext, "Unos maksimalnih bodova nije validan")
            return null;
        }

        newExamEntry.note = examNoteET.text.toString()
        newExamEntry.material = examMaterialET.text.toString()

        return newExamEntry
    }
    private fun getCriteriaObject(): List<Int> {
        val list = mutableListOf<Int>()

        if (criteria1ET.text.isNotBlank())
            list.add(criteria1ET.text.toString().toInt())

        if (criteria2ET.text.isNotBlank())
            list.add(criteria2ET.text.toString().toInt())

        if (criteria3ET.text.isNotBlank())
            list.add(criteria3ET.text.toString().toInt())

        if (criteria4ET.text.isNotBlank())
            list.add(criteria4ET.text.toString().toInt())

        if (criteria5ET.text.isNotBlank())
            list.add(criteria5ET.text.toString().toInt())

        return list
    }

    private fun pushCriteriaToDB(subjectCode: String, list: List<Int>){
        rootDBReferenceFirebase.child(subjectTreeDB).child(subjectCode).child(criteriaTreeDB).setValue(list).addOnCompleteListener {
            displayShortToast(applicationContext, "Kriteriji su obnovljeni")
        }.addOnCanceledListener {
            displayShortToast(applicationContext, R.string.actionFailed.toString())
        }
    }

    //check if the passed object is eligible to be pushed to the database
    private fun checkEligibilityExam(ex: Exam): String? {
        if (ex.earnedPoints > maxPointsValue || ex.maxPoints > maxPointsValue)
            return "Bodovi nisu validni"
        if ((ex.earnedPoints != appDefaultNullValue && ex.maxPoints != appDefaultNullValue) && (ex.earnedPoints > ex.maxPoints))
            return "Osvojeni bodovi veći od maksimalnih bodova"
        //name and date combine to make a unique identifier for every exam entry, cant be null, empty, or match another existing exams name and date
        when{
            ex.name.isBlank() -> return "Naziv ispita nije validan"
            ex.date.isBlank() -> return "Datum ispita nije validan"
        }
        if (option == OptionsEnum.AddExam) {
            listOfExistingExams.forEach {
                if (it.name == ex.name && it.date == ex.date)
                    return "Ispit sa datim imenom i datum već postoji"
            }
        }
        return null
    }
    private fun checkEligibilitySubject(sub: Subject): String? {
        val maxYear = 150
        val minYear = 0
        //name and year combine to make a unique identifier for every subject entry, cant be null, empty, or match another existing subject name and year
        when{
            sub.name.isBlank() -> return "Naziv predmeta nije validan"
            sub.year == appDefaultNullValue.toInt() || sub.year > maxYear || sub.year < minYear -> return "Godina predmeta nije validna"
        }
        if (option == OptionsEnum.AddSubject) {
            for (subject in subjectList) {
                if (sub.name == subject.name && sub.year == subject.year) {
                    return "Predmet sa datim imenom i datumom već postoji"
                }
            }
        }
        return null
    }
    private fun checkEligibilityCriteria(criteria: List<Int>): String? {
        //criteria list needs to have 5 items
        if (criteria.size != 5) {
            return "Kriteriji trebaju 5 unosa"
        }
        criteria.forEach {
            if (it > maxPointsValue)
                return "Kriteriji nisu validni"
        }
        for(i in 0..3){
            if(criteria[i] >= criteria[i+1])
                return "Raspored kriterija nije validan"
        }
        return null
    }

}