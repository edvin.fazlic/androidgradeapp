package com.example.layouttest

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class AdapterForSubject(private var mCtx: Context, private var resources: Int, private var items: List<Subject>):
    ArrayAdapter<Subject>(mCtx, resources, items) {

    @SuppressLint("ViewHolder", "SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view:View = layoutInflater.inflate(resources, null)

        val subName: TextView = view.findViewById(R.id.detailsSubjectNameTV)
        val subYear: TextView = view.findViewById(R.id.subYearTV)

        subYear.visibility = View.GONE

        val mItem: Subject = items[position]
        subName.text = mItem.name

        if (mItem.year != -1) {
            subYear.visibility = View.VISIBLE
            subYear.text = "${mItem.year + databaseSubjectYearOffset}/${mItem.year + databaseSubjectYearOffset + 1}"
        }

        return view
    }

}